/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kushchin.vitalii.hometaskshibernate;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vitalii
 */
public interface BasicDao <E, I>{
    List<E> getList();
    E create(E o);
    void update(E o, I id);
    void remove(E o, I id);
    E getByID(I id);
}
