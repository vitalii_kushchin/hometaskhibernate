/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kushchin.vitalii.hometaskshibernate;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Vitalii
 */
public class AbstractDao<E, I extends Serializable> implements BasicDao<E, I> {

    private final Class<E> someEntityClass;
    public HibernateUtil hibernateUtil;

    public AbstractDao() {
        ParameterizedType generic = (ParameterizedType) getClass().getGenericSuperclass();
        this.someEntityClass = (Class) generic.getActualTypeArguments()[0];
    }

    @Override
    public List<E> getList() {

        List list = getSession().createCriteria(someEntityClass).list();
        for (Object object : list) {
            System.out.println(object);
        }
        return list;

    }

    @Override
    public E create(E o) {
        Session session = getSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        try {
            session.save(o);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
        session.close();
        return o;

    }

    @Override
    public void update(E o, I id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
     
        transaction.begin();
        try {
            session.update(o);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            new RuntimeException("not update User!!! ", e);
        }
        session.close();

    }

    @Override
    public void remove(E o, I id) {
        Session session = getSession();
        Transaction transaction = session.getTransaction();
        Object obj = session.load(someEntityClass, id);
        transaction.begin();
        try {
            session.delete(obj);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }
        session.close();
    }

    Session getSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    @Override
    public E getByID(I id) {
        Session session = getSession();
        return  (E)session.get(someEntityClass, id);
        
    }

}
